import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import { renderRoutes } from '../imports/startup/client/routes.jsx';
import React, { Component, PropTypes } from 'react';

export const App = ({content}) => (
    <div>{content}</div>
)