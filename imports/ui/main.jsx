import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import React, { Component, PropTypes } from 'react';

export const App = ({content}) => (
    <div className="col-md-12">{content}</div>
)