import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

export default class Row extends Component {

    renderContent() {
        if (this.props.isHTML) { return this.renderHTML(); }
        else { return this.renderString(); }
    }

    renderHTML() {
        return (
            <div dangerouslySetInnerHTML={this.createMarkup(this.props.content) }></div>
        )
    }

    renderString() {
        return (
            <div>{this.props.content}</div>
        )
    }

    createMarkup(data) {
        return { __html: data };
    }
    render() {
        return (
            <tr>
                <td>
                    <div className="pull-right">{this.props.title}</div>
                </td>
                <td>
                    {this.renderContent()}
                </td>
            </tr>
        )
    }
} 