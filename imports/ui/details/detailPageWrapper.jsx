import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import LoginForm from '../login/components/form';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { createContainer } from 'meteor/react-meteor-data';
import { Template } from 'meteor/templating';
import { Blaze } from 'meteor/blaze';

import { Events } from '../../api/events';
import Row from './components/row.jsx';

class DetailPageWrapper extends Component {
    
    isPrepared(){
        return this.props.events;
    }
    
    createMarkup() { return {__html: 'First &middot; Second'}; };
    
    convertTime(inputDateTime){
        var options = {
            weekday: "long", year: "numeric", month: "short",
            day: "numeric", hour: "2-digit", minute: "2-digit"
        };
        
        return new Date(inputDateTime).toLocaleTimeString('en-sg', options)
    }
    
    renderEvents(){
        return this.props.events.map((event)=>(
            <div>
            {console.log(event)}
                <div className="page-header">
                    <h1>{event.title}</h1>
                </div>
                <div className="table-responsive">
                <table className="table">
                    <tbody>
                        <Row title="Title : " content={event.title} />
                        <Row title="Organizer : " content={event.organizer} />
                        <Row title="Committee : " content={event.committee.title} />
                        <Row title="Category : " content={event.category.title} />
                        <Row title="Tags : " content={event.tags.map((tag)=>(<span>{tag}, </span>))} />
                        <Row title="Start : " content={this.convertTime(event.start).toString()} />
                        <Row title="End : " content={this.convertTime(event.end).toString()} />
                        <Row title="Description : " isHTML={true} content={event.description} />
                        <Row title="Date and Time : " content={event.datetime} />
                        <Row title="Venue : " content={event.venue} />
                        <Row title="Price : " content={event.price} />
                        <Row title="Agenda : " content={event.agenda} />
                        <Row title="Contact : " content={event.contact} />
                    </tbody>
                </table>
                </div>
            </div>
        ));
    }
    
    handleClose(event) {
        event.preventDefault();
        window.close();
    }
    
    render() {
        //console.log(this.props.params);
        Tracker.autorun(function () {
            if (!Meteor.loggingIn()&&!Meteor.user()) {
                //FlowRouter.redirect('/'); 
            }
        });
        return (
            
            <div>
                {this.renderEvents()}
                <div onClick={this.handleClose} className="btn btn-danger">Close</div>
            </div>
            
        );
    }
}


DetailPageWrapper.propTypes = {
    events: PropTypes.array.isRequired,
};

export default createContainer(({params})=>{
    return {
        events:Events.find({_id:params.id}).fetch(),
    };
}, DetailPageWrapper);

//console.log(DetailPageWrapper.propTypes);
