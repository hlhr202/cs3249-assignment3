import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Events } from '../../api/events.js';
import LoginForm from '../login/components/form';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { createContainer } from 'meteor/react-meteor-data';

import TableWrapper from './components/table.jsx';

class ListPageWrapper extends Component {

    constructor(props) {
        super(props);
        this.state = {
            autoUpdate: true,
        }
    }

    componentDidMount() {
        this.renderList(this.props.events, false);
    }

    componentDidUpdate() {
        if (this.state.autoUpdate) {
            this.renderList(this.props.events, false);
        } else {
            return;
        }
    }

    disableAutoUpdate() {
        this.state.autoUpdate = false;
    }

    renderList(eventList, releaseChildUpdate) {
        if (!eventList) return;
        ReactDOM.render(<TableWrapper ref="tableWrapper" releaseAutoUpdate={releaseChildUpdate} disableAutoUpdate={this.disableAutoUpdate.bind(this) } eventList={eventList}/>,
            document.getElementById("table-data"));
    }

    handleSearch() {
        this.state.autoUpdate = false;
        var querystring = this.refs.search.value;
        var events = Events.find({ $or: [{ title: { $regex: querystring, $options: 'i' } }, { organizer: { $regex: querystring, $options: 'i' } }] }).fetch();
        this.renderList(events, true);
    }

    handleRevert() {
        this.state.autoUpdate = true;
        var events = Events.find().fetch();
        this.renderList(events, true);
    }

    render() {
        DocHead.setTitle("All Event List");
        Tracker.autorun(function () {
            if (!Meteor.loggingIn() && !Meteor.user()) {
                FlowRouter.redirect('/');
            }
        });

        return (
            <div>
                <LoginForm />
                <div className="page-header">
                    <h1>{DocHead.getTitle() }</h1>
                </div>
                <div className="row">
                    <div className="form-group col-md-3">

                        <a href="/createevent" target="_blank" className="btn btn-success">Create New</a>
                    </div>
                    <div className="col-md-9">
                        <div className="pull-right">
                            <div className="form-inline col-md-12">
                                <div className="form-group">

                                    <input name="search" className="form-control" ref="search"/> 
                                    <div onClick={this.handleSearch.bind(this) } className="btn btn-primary form-control">Search</div> 
                                    
                                </div>
                                <div onClick={this.handleRevert.bind(this) } className="btn btn-warning form-control">Revert Search</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="table-data" ref="table" className="table-responsive"></div>



            </div>
        );
    }
}

ListPageWrapper.propTypes = {
    events: PropTypes.array.isRequired,
};

export default createContainer(() => {
    return {
        events: Events.find().fetch(),
    };
}, ListPageWrapper);