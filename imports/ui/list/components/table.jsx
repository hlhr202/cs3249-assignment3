import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Row from './row.jsx';
'use strict';

class List extends Component {
    renderList(eventList){
        return eventList.map((event)=>(
            <Row event={event} />
        ));
    }
    render(){
        var eventList = this.props.eventList;
        return (
            <table className="table table-hover">
            <thead>
            <tr>
                <td>
                    <label onClick={this.props.sortByTitle}>Event Title</label>
                </td>
                <td>
                    <label onClick={this.props.sortByOrganizer}>Event Organizer</label>
                </td>
                <td>
                    <label>Event Date Time</label>
                </td>
            </tr>
            </thead>
            <tbody>
                {this.renderList(eventList)}
            </tbody>
            </table>
        )
    }
}

export default class TableWrapper extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            originalList:props.eventList,
            autoUpdate:true
        }
    }
    
    releaseAutoUpdate(){
        this.state.autoUpdate = true;
    }
    
    componentDidMount(){
        this.renderContent(this.props.eventList);
    }
    
    componentDidUpdate(){
        if (this.props.releaseAutoUpdate){
            this.releaseAutoUpdate();
        }
        if (this.state.autoUpdate){
            this.renderContent(this.props.eventList);
        }
        else {
            return;
        }
    }
    
    sortByTitle(){
        this.props.disableAutoUpdate();
        this.state.autoUpdate = false;
        var sortedList = this.props.eventList.sort(function(a, b){
            return a.title.localeCompare(b.title);
        });
        this.renderContent(sortedList);
    }
    
    sortByOrganizer(){
        this.props.disableAutoUpdate();
        this.state.autoUpdate = false;
        var sortedList = this.props.eventList.sort(function(a, b){
            return a.organizer.localeCompare(b.organizer);
        });
        this.renderContent(sortedList);
    }
    
    renderContent(eventList){
        ReactDOM.render(<List sortByOrganizer={this.sortByOrganizer.bind(this)} sortByTitle={this.sortByTitle.bind(this)} eventList={this.props.eventList} />,
            document.getElementById('eventList'));
    }
    
    render(){
        return (
            <div id="eventList">
            </div>
        )
    }
}