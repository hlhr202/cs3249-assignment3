import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

export default class Row extends Component{
    render(){
        return (
            <tr>
                <td><a href={"javascript:window.open('event/"+this.props.event._id+"','mywindowtitle','width=600,height=800')"}>{this.props.event.title}</a></td>
                <td>{this.props.event.organizer}</td>
                <td>{this.props.event.datetime}</td>
            </tr>
        )
    }
}