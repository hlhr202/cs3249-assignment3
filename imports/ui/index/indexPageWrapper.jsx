import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Template } from 'meteor/templating';
import { Blaze } from 'meteor/blaze';
import LoginForm from '../login/components/form';
import { FlowRouter } from 'meteor/kadira:flow-router';

export default class IndexPageWrapper extends Component{

  render() {
    // Just render a placeholder container that will be filled in

    Tracker.autorun(function(){
        if(Meteor.user()){
            FlowRouter.redirect("/eventlist");
        }
    });
    
    DocHead.setTitle("Welcome");
    
    return (
        <div>
            <LoginForm />
            <div className="container">
                <div className="jumbotron">
                    <h1>Welcome</h1>
                    <br />
                    <p>This is a simple event list system</p>
                    <p>Signup & Login from left top</p>
                </div>
            </div>
        </div>
    );
    
  }
}