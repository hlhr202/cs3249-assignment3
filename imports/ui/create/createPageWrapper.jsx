import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import LoginForm from '../login/components/form';
import { FlowRouter } from 'meteor/kadira:flow-router';
import { createContainer } from 'meteor/react-meteor-data';
import { Template } from 'meteor/templating';
import { Blaze } from 'meteor/blaze';

import { Events } from '../../api/events';

import TextInputBox from './components/textInputBox.jsx';
import DateTimeInputBox from './components/dateTimeInputBox.jsx';
import SelectInputBox from './components/selectInputBox.jsx';
import HTMLInputBox from './components/HTMLInputBox.jsx';


export default class CreatePageWrapper extends Component {

    handleClose(event) {
        event.preventDefault();
        window.close();
    }

    handleSubmit(event) {

        event.preventDefault();

        var newEvents = {};
        newEvents.committee = {};
        newEvents.category = {};

        newEvents.creator = Meteor.user()._id;

        try {
            newEvents.title = this.refs.title.getText();

            newEvents.organizer = this.refs.organizer.getText();
            newEvents.description = this.refs.description.getHTML();
            newEvents.committee.value = this.refs.committee.getValue();
            newEvents.committee.title = this.refs.committee.getTitle();
            newEvents.category.value = this.refs.category.getValue();
            newEvents.category.title = this.refs.category.getTitle();
            newEvents.tags = this.refs.tags.getText().split(',');
            newEvents.start = this.refs.start.getDateTime();
            newEvents.end = this.refs.end.getDateTime();

            newEvents.datetime = this.refs.datetime.getText();
            newEvents.venue = this.refs.venue.getText();
            newEvents.price = this.refs.price.getText();
            newEvents.agenda = this.refs.agenda.getText();
            newEvents.contact = this.refs.contact.getText();
            newEvents.createdAt = new Date();
        } catch (e) {
            console.log(e);
            alert(e.message);
            return;
        }

        if (Meteor.user()) {
            var id = Events.insert(newEvents);
            if (id) {
                alert("Create Event completed!");
            }
        }


    }



    render() {
        DocHead.setTitle("Create New Event");
        Tracker.autorun(function (c) {
            if (!Meteor.loggingIn() && !Meteor.user()) {
                c.stop();
                //window.location = '/';
                FlowRouter.redirect('/');
            }
        });
        var committee = [
            { value: "test", title: "Test", key: 1 }
        ];
        var category = [
            { value: "testCategory", title: "TestCategory", key: 1 },
            { value: "testCategory2", title: "TestCategory2", key: 2 }
        ]
        return (
            <div>
                <LoginForm />
                <div className="page-header">
                    <h1>{DocHead.getTitle() }</h1>
                </div>
                <form onSubmit={this.handleSubmit.bind(this) }>
                    <div className="row">

                        <div className="table-responsive">
                            <table className="table table-hover">
                                <tbody>
                                    <TextInputBox ref="title" title="Title" name="title" requiredArea={true}/>
                                    <TextInputBox ref="organizer" title="Organizer" name="organizer" requiredArea={true}/>
                                    <SelectInputBox ref="committee" title="Committee" name="committee" options={committee}/>
                                    <SelectInputBox ref="category" title="Category" name="category" options={category}/>
                                    <TextInputBox ref="tags" title="Tags" name="tags"/>
                                    <DateTimeInputBox ref="start" title="Start" name="start"/>
                                    <DateTimeInputBox ref="end" title="End" name="end"/>
                                    <HTMLInputBox ref="description" title="Description"></HTMLInputBox>
                                    <TextInputBox ref="datetime" title="Date & Time" name="datetime"/>
                                    <TextInputBox ref="venue" title="Venue" name="venue"/>
                                    <TextInputBox ref="price" title="Price" name="price"/>
                                    <TextInputBox ref="agenda" title="Agenda" name="agenda"/>
                                    <TextInputBox ref="contact" title="Contact" name="contact"/>
                                    <tr>
                                        <td>
                                            <div className="form-group">
                                                <div className="col-md-12 text-right">
                                                    <div onClick={this.handleClose} className="btn btn-danger">Close</div>
                                                    <button type="submit" className="btn btn-primary">Create</button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>


                    </div>

                </form>
            </div>

        );

    }
}