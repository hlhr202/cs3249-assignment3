import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

class RequiredNullException{
    constructor(message){
        this.message = message;
        this.name = "RequiredNullException";
    }
}

export default class TextInputBox extends Component{
    
    getText(){
        if (this.props.requiredArea && !this.refs.text.value) {
            throw new RequiredNullException(this.props.title + " is empty!");
        }
        else return this.refs.text.value;
    }
    render(){
        var requireStar = null;
        if (this.props.requiredArea){
            requireStar = <span style={{color:"red"}}>* </span>
        }
        return(
            <tr>
                <td>
                    <div className="form-group">
                        
                        <label className="col-md-3 text-right">{requireStar} {this.props.title}</label>
                        <div className="col-md-9">
                            <input className="form-control" type="text" ref="text" name={this.props.name}/>
                        </div>
                    </div>
                </td>
            </tr>
        );
    }
}