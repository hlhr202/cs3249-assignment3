import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

export default class DateTimeInputBox extends Component{
    getDateTime(){
        return this.refs.datetime.value;
    }
    render(){
        return(
            <tr>
                <td>
                    <div className="form-group">
                        <label className="col-md-3 text-right">{this.props.title}</label>
                        <div className="col-md-9">
                            <input className="form-control" ref="datetime" type="datetime-local" name={this.props.name}/>
                        </div>
                    </div>
                </td>
            </tr>
        );
    }
}