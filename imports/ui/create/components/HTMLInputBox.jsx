import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Template } from 'meteor/templating';
import { Blaze } from 'meteor/blaze';

import "./froalaTemplate.html";
import "./froalaTemplate.css";
'use strict';

var htmlData = {};
htmlData.get = function(){return null};

Template.myTemplate.helpers({
    getFEContext:function(){
        return {
            height:300,
            _value:"",
            _onfocus: function(e, editor){
                htmlData = editor.html;
                window.htmlData = editor.html;
            },
        }
    },
    
    /*
    initBox: function(){
        return function(e, editor){
            console.log(editor);
            window.htmlData = editor.html;
        }
    },*/
    
});

export default class HTMLInputBox extends Component {
    
    componentDidMount(){
        this.view = Blaze.render(Template.myTemplate, ReactDOM.findDOMNode(this.refs.froala));
    }
    
    getHTML(){
        return htmlData.get(true);
    }
    render() {
        return (
            <tr>
                <td>
                    <div className="form-group">
                        <label className="col-md-3 text-right">{this.props.title}</label>
                        <div className="col-md-9" ref="froala"></div>
                    </div>
                </td>
            </tr>
        )
    }
}