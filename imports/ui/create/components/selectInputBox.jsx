import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

export default class SelectInputBox extends Component{
    getValue(){
        return this.refs.select.value;
    }
    getTitle(){
        return this.refs.select[this.refs.select.selectedIndex].text;
    }
    render(){
        var options = [];
        for (var i = 0; i < this.props.options.length; i++){
            options.push(<option value={this.props.options[i].value} key={this.props.options[i].key}>{this.props.options[i].title}</option>)
        } 
        return(
            <tr>
                <td>
                    <div className="form-group">
                        <label className="col-md-3 text-right">{this.props.title}</label>
                        <div className="col-md-9">
                            <select className="form-control" ref="select" name={this.props.name}>{options}</select>
                        </div>
                    </div>
                </td>
            </tr>
        );
    }
}