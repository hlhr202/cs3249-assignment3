import { FlowRouter } from 'meteor/kadira:flow-router';
import { mount } from 'react-mounter';
import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

// route components
import IndexPageWrapper from '../../ui/index/indexPageWrapper.jsx';
import ListPageWrapper from '../../ui/list/listPageWrapper.jsx';
import CreatePageWrapper from '../../ui/create/createPageWrapper.jsx';
import DetailPageWrapper from '../../ui/details/detailPageWrapper.jsx';
import {App} from '../../ui/main.jsx';

FlowRouter.route('/', {
  name:'index',
  action() {
    mount(App,{
      content:(<IndexPageWrapper />)
    });
  }
});

FlowRouter.route('/eventlist', {
  name:'eventlist',
  onBeforeAction:function(){
    return document.title = "Event List";
  },
  action(){
    mount(App,{
      content:(<ListPageWrapper />)
    });
  }
});

FlowRouter.route('/createevent',{
  name:'createevent',
  action(){
    mount(App,{
      //main:()=><App />
      content:(<CreatePageWrapper />)
    });
  }
});

FlowRouter.route('/event/:id', {
  name:'event',
  action(params, queryParams){
    mount(App,{
      content:(<DetailPageWrapper params={params} query={queryParams}/>)
    })
  }
})