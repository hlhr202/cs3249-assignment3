import { Accounts } from 'meteor/accounts-base';
import { FlowRouter } from 'meteor/kadira:flow-router';
Accounts.ui.config({
    passwordSignupFields:'USERNAME_AND_EMAIL'
});